var foods = [
	{
		"id":1,
		"name": "Tomyam Kung",
		"type": "Spicy Thai",
		"price": "150"
	},
	{
		"id":2,
		"name": "Pad Thai",
		"type": "Thai food",
		"price": "80"
	},
	{
		"id":3,
		"name": "Larb",
		"type": "Thai food",
		"price": "80"
	},
	{
		"id":4,
		"name": "Pad Prik",
		"type": "Thai food",
		"price": "80"
	},
	{
		"id":5,
		"name": "Pad Ka Prao",
		"type": "Spicy Thai",
		"price": "80"
	}
];

exports.findAll = function() {
	return foods;
};

exports.findById = function (id) {
	for (var i = 0; i < foods.length; i++) {
		if (foods[i].id == id) return foods[i];
	}
};
exports.foods = foods;