const express = require('express')
var path = require('path');
var fs = require('fs');
const app = express()

var bodyParser = require('body-parser');
// import foods from('./Menu_food');
var menuFood = require('./Menu_food');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use('/cssFiles',express.static(__dirname + '/assets'));



app.get('/',(req, res) => {
    res.sendFile('index.html',{root: path.join(__dirname,'./files')});
    // res.send('Hello World :home')
});
app.post('/',(req, res) => {
    // res.sendFile('index.html',{root: Path2D.join(__dirname,'./files')});
    // res.send('Hello World :home')
    // res.end(JSON.stringify(req.body));
    if(req.body.username == 'admin' && req.body.password == 'admin123'){
        // res.end(JSON.stringify(req.body.username));
        res.sendFile('control.html',{root: path.join(__dirname,'./files')});
    }
});

app.get('/food', function (req, res) {
	res.json(menuFood.findAll());
});

app.get('/food/:id', function (req, res) {
	var id = req.params.id;
	res.json(menuFood.findById(id));
});

// app.post('/newfood', function (req, res) {
//     var json = req.body;
//     res.send('Add new ' + json.name + ' Completed!');

// });
app.get('/newfood',(req, res) => {
    res.sendFile('newfood.html',{root: path.join(__dirname,'./files')});
});
app.post('/newfood', (req, res) => {
    const food = {
        id: menuFood.foods.length + 1,
        name: req.body.name,
        type: req.body.typeMovie,
        price: req.body.price
        //? "id":1,
		//? "name": "Tomyam Kung",
		//? "type": "Spicy Thai",
		//? "price": "150"
    };
    menuFood.foods.push(food);
    res.send(food);
});




app.get('/control',(req, res) => {
    res.sendFile('control.html',{root: path.join(__dirname,'./files')});
});
app.post('/control',(req, res) => {
    // res.sendFile('index.html',{root: Path2D.join(__dirname,'./files')});
    // res.send('Hello World :home')
    // res.end(JSON.stringify(req.body));
    if(req.body.i=='add_newmenu'){
        // res.end(JSON.stringify(req.body.username));
        res.sendFile('newfood.html',{root: path.join(__dirname,'./files')});
    }
    if(req.body.i=='menu'){
        res.json(menuFood.findAll());
    }

});

app.listen(3000, () => console.log('Example app listening on port 3000!'))
