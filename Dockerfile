 FROM node:9

 MAINTAINER Fsuresure

 LABEL "version"="1.0.0"

 RUN mkdir -p /var/www/app

 WORKDIR /var/www/app

 COPY . .
 RUN npm install

 EXPOSE 3000

 CMD ["node", "index.js"];